package com.agileai.wm.module.workmanage.handler;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.module.workmanage.service.WmWeekManage;

public class MobileWeekManageProviderHandler extends SimpleHandler{
	public static String ERROR = "error";
	public MobileWeekManageProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param){
		String responseText = FAIL;
		return new AjaxRenderer(responseText);
	}
	@PageAction
	public ViewRenderer findWeekManageCardInfo(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row1 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.getString("WT_ID");
			}
			DataRow row2 = wmWeekManage.getBeforeWeekRowInfo(currentWeekId);
			String beforeWeekId ="";
			if(row2 != null){
				beforeWeekId = row2.getString("WT_ID");
			}
			
			JSONObject jsonObject = new JSONObject();
			List<DataRow> beforeWeekRecords = wmWeekManage.getWeekWorkRecord(userId,beforeWeekId);
			JSONArray jsonArray1 = new JSONArray();
			if(beforeWeekRecords.size() != 0){
				int size = 5;
				if(beforeWeekRecords.size() < 5){
					size = beforeWeekRecords.size(); 
				}
				for(int i=0;i<size;i++){
					DataRow row = beforeWeekRecords.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject1.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonObject1.put("finish", row.stringValue("ENTRY_FINISH"));
					jsonObject1.put("num", i+1);
					jsonArray1.put(jsonObject1);
				}
			}else{
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("num",1);
				jsonObject1.put("describe", "无");
				jsonObject1.put("planday", "0");
				jsonArray1.put(jsonObject1);
			}
			
			List<DataRow> currentWeekRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray jsonArray2 = new JSONArray();
			if(currentWeekRecords.size() != 0){
				int size = 5;
				if(currentWeekRecords.size() < 5){
					size = currentWeekRecords.size(); 
				}
				for(int i=0;i<size;i++){
					DataRow row = currentWeekRecords.get(i);
					JSONObject jsonObject2 = new JSONObject();
					jsonObject2.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject2.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonObject2.put("num", i+1);
					jsonArray2.put(jsonObject2);
				}
			}else{
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("num", 1);
				jsonObject2.put("describe", "无");
				jsonObject2.put("planday", "0");
				jsonArray2.put(jsonObject2);
			}
			
			List<DataRow> nextWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,currentWeekId);
			JSONArray jsonArray3 = new JSONArray();
			if(nextWeekRecords.size() != 0){
				int size = 5;
				if(nextWeekRecords.size() < 5){
					size = nextWeekRecords.size(); 
				}
				for(int i=0;i<size;i++){
					DataRow row = nextWeekRecords.get(i);
					JSONObject jsonObject3 = new JSONObject();
					jsonObject3.put("describe", row.stringValue("PRE_DESCRIBE"));
					jsonObject3.put("planday", row.stringValue("PRE_LOAD"));
					jsonObject3.put("num", i+1);
					jsonArray3.put(jsonObject3);
				}
			}else{
				JSONObject jsonObject3 = new JSONObject();
				jsonObject3.put("num", 1);
				jsonObject3.put("describe", "无");
				jsonObject3.put("planday", "0");
				jsonArray3.put(jsonObject3);
			}
			
			jsonObject.put("beforeWeek", jsonArray1);
			jsonObject.put("currentWeek", jsonArray2);
			jsonObject.put("follow", jsonArray3);
			jsonObject.put("beforeCount", beforeWeekRecords.size());
			jsonObject.put("currentCount", currentWeekRecords.size());
			jsonObject.put("followCount", nextWeekRecords.size());
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findLastWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		try {
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String beforeDate = param.get("time");
			if(beforeDate == null || beforeDate.isEmpty() || "null".equals(beforeDate) ){
				beforeDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(new Date()), DateUtil.DAY, -5));
			}else{
				beforeDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(DateUtil.getDateTime(beforeDate)), DateUtil.DAY, -5));
			}
			DataRow row1 = wmWeekManage.getCurrentWeek(beforeDate);
			String beforeWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				beforeWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				
				DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,beforeWeekId);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", startTime);
				jsonObject.put("time", startTime);
				jsonObject.put("endTime", endTime);
				if(weekManageRow != null){
					jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
				}else{
					jsonObject.put("wwDay", "0");
				}
				List<DataRow> beforeWeekRecords = wmWeekManage.getWeekWorkRecord(userId,beforeWeekId);
				JSONArray jsonArray11 = new JSONArray();
				if(beforeWeekRecords.size() != 0){
					for(int i=0;i<beforeWeekRecords.size();i++){
						DataRow row = beforeWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
						jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
						jsonArray11.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", "无");
					jsonObject11.put("planday", "0");
					jsonArray11.put(jsonObject11);
				}
				
				List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,beforeWeekId);
				JSONArray jsonArray12 = new JSONArray();
				if(prepareWeekRecords.size() != 0){
					for(int i=0;i<prepareWeekRecords.size();i++){
						DataRow row = prepareWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
						jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
						jsonArray12.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", "无");
					jsonObject11.put("preplanday","0");
					jsonArray12.put(jsonObject11);
				}
				jsonObject.put("plan", jsonArray11);
				jsonObject.put("follow", jsonArray12);
				responseText = jsonObject.toString();
			}else{
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", ERROR);
				responseText = jsonObject.toString();
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findThisWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		try {
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row1 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}
			DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("startTime", startTime);
			jsonObject.put("time", startTime);
			jsonObject.put("endTime", endTime);
			if(weekManageRow != null){
				jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
			}else{
				jsonObject.put("wwDay", "0");
			}
			List<DataRow> beforeWeekRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray jsonArray11 = new JSONArray();
			if(beforeWeekRecords.size() != 0){
				for(int i=0;i<beforeWeekRecords.size();i++){
					DataRow row = beforeWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("describe", "无");
				jsonObject11.put("planday", "0");
				jsonArray11.put(jsonObject11);
			}
			
			List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,currentWeekId);
			JSONArray jsonArray12 = new JSONArray();
			if(prepareWeekRecords.size() != 0){
				for(int i=0;i<prepareWeekRecords.size();i++){
					DataRow row = prepareWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
					jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
					jsonArray12.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("predesc", "无");
				jsonObject11.put("preplanday", "0");
				jsonArray12.put(jsonObject11);
			}
			jsonObject.put("plan", jsonArray11);
			jsonObject.put("follow", jsonArray12);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findFllowWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		try {
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String followDate = param.get("time");
			if(followDate == null|| followDate.isEmpty() || "null".equals(followDate)){
				followDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(new Date()), DateUtil.DAY, 9));
			}else{
				followDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(DateUtil.getDateTime(followDate)), DateUtil.DAY, 9));
			}
			DataRow row1 = wmWeekManage.getCurrentWeek(followDate);
			String followWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				followWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}
			DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,followWeekId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("startTime", startTime);
			jsonObject.put("time", startTime);
			jsonObject.put("endTime", endTime);
			if(weekManageRow != null){
				jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
			}else{
				jsonObject.put("wwDay", "0");
			}
			
			List<DataRow> followWeekRecords = wmWeekManage.getWeekWorkRecord(userId,followWeekId);
			JSONArray jsonArray11 = new JSONArray();
			if(followWeekRecords.size() != 0){
				for(int i=0;i<followWeekRecords.size();i++){
					DataRow row = followWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("describe", "无");
				jsonObject11.put("planday", "0");
				jsonArray11.put(jsonObject11);
			}
			
			List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,followWeekId);
			JSONArray jsonArray12 = new JSONArray();
			if(prepareWeekRecords.size() != 0){
				for(int i=0;i<prepareWeekRecords.size();i++){
					DataRow row = prepareWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
					jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
					jsonArray12.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("predesc", "无");
				jsonObject11.put("preplanday", "0");
				jsonArray12.put(jsonObject11);
			}
			jsonObject.put("plan", jsonArray11);
			jsonObject.put("follow", jsonArray12);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWorkCompleteCardInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -21));
			DataRow row2 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row2 != null && row2.size() > 0){
				currentWeekId = row2.stringValue("WT_ID");
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONArray jsonArray4 = new JSONArray();
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				jsonArray1.put(row.stringValue("USER_NAME"));
				String userId = row.getString("USER_ID");
				List<DataRow> records = wmWeekManage.findweekCompRecords(sdate,currentDate,userId);
				int weekComp = 0;
				int weekCompAvg = 0;
				for(int j=0;j<records.size();j++){
					DataRow comRow = records.get(j);
					weekComp = weekComp + Integer.valueOf(comRow.stringValue("WW_COMPLETION"));
					int count = records.size();
					if(count == 0){
						count = 1;
					}
					weekCompAvg = weekComp/count;
				}
				jsonArray2.put(weekCompAvg);
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
				if(weekWorkRow != null){
					jsonArray3.put(Integer.valueOf(weekWorkRow.stringValue("WW_COMPLETION")));
				}else{
					jsonArray3.put(0);
				}
			}
			jsonArray4.put(jsonArray2);
			jsonArray4.put(jsonArray3);
			jsonObject.put("labels", jsonArray1);
			jsonObject.put("data", jsonArray4);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findStatisCurrentWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row2 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row2 != null && row2.size() > 0){
				currentWeekId = row2.stringValue("WT_ID");
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,currentWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("time",currentDate);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisBeforeWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String beforeDate = param.get("time");
			if(beforeDate == null || beforeDate.isEmpty() || "null".equals(beforeDate) ){
				beforeDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(new Date()), DateUtil.DAY, -5));
			}else{
				beforeDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(DateUtil.getDateTime(beforeDate)), DateUtil.DAY, -5));
			}
			DataRow row1 = wmWeekManage.getCurrentWeek(beforeDate);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			String beforeWeekId = "";
			if(row1 != null && row1.size() > 0){
				beforeWeekId = row1.stringValue("WT_ID");
			}else{
				jsonObject.put("week",ERROR);
			}
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,beforeWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,beforeWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("time",beforeDate);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findStatisFollowWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String nextDate = param.get("time");
			if(nextDate == null|| nextDate.isEmpty() || "null".equals(nextDate)){
				nextDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(new Date()), DateUtil.DAY, 9));
			}else{
				nextDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(DateUtil.getBeginOfWeek(DateUtil.getDateTime(nextDate)), DateUtil.DAY, 9));
			}
			DataRow row1 = wmWeekManage.getCurrentWeek(nextDate);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			String nextWeekId = "";
			if(row1 != null && row1.size() > 0){
				nextWeekId = row1.stringValue("WT_ID");
			}else{
				jsonObject.put("week",ERROR);
			}
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,nextWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,nextWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("time",nextDate);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userId", row.stringValue("USER_ID"));
				jsonObject1.put("userName",row.stringValue("USER_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("userInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWeekWorkCardInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray jsonArray1 = new JSONArray();
			
			String edate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String beginDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -60));
			List<DataRow> records = wmWeekManage.findweekCompRecords(beginDate,edate,userId);
			for(int i=0;i<records.size();i++){
				DataRow row = records.get(i);
				String weekComp = row.stringValue("WW_COMPLETION");
				jsonArray1.put(weekComp);
			}
			if(records.size() < 8){
				int size = 8 - records.size();
				for(int i=0;i<size;i++){
					jsonArray1.put("0");
				}
			}
			
			jsonArray.put(jsonArray1);
			jsonObject.put("data", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWeekWorkDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -60));
			String edate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			List<DataRow> records = wmWeekManage.findweekCompRecords(sdate,edate,userId);
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject1 = new JSONObject();
				DataRow row = records.get(i);
				String weekComp = row.stringValue("WW_COMPLETION");
				String beginDate = row.stringValue("WT_BEGIN").substring(5, 10).replaceAll("-", "/");
				String endDate = row.stringValue("WT_END").substring(5, 10).replaceAll("-", "/");
				jsonObject1.put("beginDate", beginDate);
				jsonObject1.put("endDate", endDate);
				if(!weekComp.isEmpty()){
					jsonObject1.put("comp", weekComp);
				}else{
					jsonObject1.put("comp", "0");
				}
				
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("weekWork", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initWeekExaminationUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> userRecords = wmWeekManage.findWeekExaminationUserInfos(grpId);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userId", row.stringValue("USER_ID"));
				jsonObject1.put("userName",row.stringValue("USER_NAME"));
				jsonObject1.put("userCode", row.stringValue("USER_CODE"));
				jsonArray1.put(jsonObject1);
				jsonArray2.put(row.stringValue("USER_CODE"));
			}
			jsonObject.put("userInfos", jsonArray1);
			jsonObject.put("userCodes", jsonArray2);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initWeekExamGroupInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> groupRecords = wmWeekManage.initGroupRecords(userId);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<groupRecords.size();i++){
				DataRow row = groupRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("grpId", row.stringValue("GRP_ID"));
				jsonObject1.put("grpName",row.stringValue("GRP_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("groupInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findActiveUserId(DataParam param){
		String responseText = FAIL;
		try {
			String userCode = param.get("userCode");
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			DataRow userInfo = wmWeekManage.findActiveUserId(userCode);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("userId", userInfo.stringValue("USER_ID"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
}
