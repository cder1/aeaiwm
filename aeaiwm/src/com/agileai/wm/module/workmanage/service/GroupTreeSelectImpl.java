package com.agileai.wm.module.workmanage.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.wm.module.workmanage.service.GroupTreeSelect;

public class GroupTreeSelectImpl
        extends TreeSelectServiceImpl
        implements GroupTreeSelect {
    public GroupTreeSelectImpl() {
        super();
    }
}
