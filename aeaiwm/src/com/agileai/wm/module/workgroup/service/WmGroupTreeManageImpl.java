package com.agileai.wm.module.workgroup.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class WmGroupTreeManageImpl
        extends TreeManageImpl
        implements WmGroupTreeManage {
    public WmGroupTreeManageImpl() {
        super();
        this.idField = "GRP_ID";
        this.nameField = "GRP_NAME";
        this.parentIdField = "GRP_PID";
        this.sortField = "GRP_NUMBER";
    }
    
	public void insertChildRecord(DataParam param) {
		String parentId = param.get(idField);
		String newMenuSort = String.valueOf(this.getNewMaxSort(parentId));
		param.put("CHILD_"+sortField,newMenuSort);
		param.put("CHILD_"+idField,KeyGenerator.instance().genKey());
		param.put("CHILD_"+parentIdField,param.get(idField));
		DataParam proccessParam = processEmptyDataParam(param);
		String statementId = this.sqlNameSpace+"."+"insertTreeRecord";
		this.daoHelper.insertRecord(statementId, proccessParam);
	}	
	
	protected DataParam processEmptyDataParam(DataParam param){
		DataParam result = new DataParam();
		Iterator<String> keys = param.keySet().iterator();
		while(keys.hasNext()){
			String key = keys.next();
			Object value = param.get(key);
			if (value != null && (value instanceof String) && String.valueOf(value).trim().equals("")){
				continue;
			}
			result.put(key,value);
		}
		return result;
	}

	@Override
	public void addRelation(String grpId, List<String> userIdList) {
		String statementId = this.sqlNameSpace+"."+"addRelation";
		if (userIdList.size() > 0){
			List<DataParam> paramList = new ArrayList<DataParam>();
			for (int i=0;i < userIdList.size();i++){
				DataParam param = new DataParam();
				param.put("USER_ID",userIdList.get(i),"GRP_ID",grpId);
				paramList.add(param);
			}
			this.daoHelper.batchInsert(statementId, paramList);
		}
	}

	@Override
	public void removeRelation(String grpId, String userId) {
		String statementId = this.sqlNameSpace+"."+"removeRelation";
		DataParam param = new DataParam();
		param.put("USER_ID",userId,"GRP_ID",grpId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	public void deleteCurrentRecord(String currentId) {
		String statementId = this.sqlNameSpace+"."+"deleteTreeRecord";
		this.daoHelper.deleteRecords(statementId, currentId);
		
		statementId = this.sqlNameSpace+"."+"removeRelation";
		DataParam param = new DataParam();
		param.put("GRP_ID",currentId);
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	@Override
	public void updateRelProperties(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"updateRelProperties";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> findRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow findDetail(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findDetail";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
