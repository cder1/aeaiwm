package com.agileai.wm.module.daywork.handler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.module.daywork.service.WmDayworkManage;
import com.agileai.wm.module.workmanage.service.WmWeekManage;

public class MobileDailyManageProviderHandler extends SimpleHandler{
	private static String ERROR = "error";
	public MobileDailyManageProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param){
		String responseText = FAIL;
		return new AjaxRenderer(responseText);
	}
	@PageAction
	public ViewRenderer findDailyManageCardInfo(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			String currentTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			
			JSONObject jsonObject = new JSONObject();
			DataRow row2 = wmDayworkManage.findCurrentDayworkRecords(userId,currentTime);
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject2 = new JSONObject();
			if(row2 != null && row2.size() > 0){
				jsonObject2.put("id", row2.stringValue("TW_ID"));
				jsonObject2.put("content", row2.stringValue("TW_CONTENT"));
			}else{
				jsonObject2.put("content", "无");
			}
			jsonArray.put(jsonObject2);
			
			jsonObject.put("today", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findWorkDailiesListInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			String minTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			String currentTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String maxTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1));
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			
			List<DataRow> pastDayworkRecords = wmDayworkManage.findPastDayWorkRecords(userId,minTime);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			if(pastDayworkRecords.size() != 0){
				int size = 5;
				if(pastDayworkRecords.size() < 5){
					size = pastDayworkRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = pastDayworkRecords.get(i);
					JSONObject jsonObject1 = new JSONObject();
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					jsonObject1.put("time", row.stringValue("TW_TIME").substring(0, 10));
					jsonObject1.put("weekText", weekText);
					jsonObject1.put("id", row.stringValue("TW_ID"));
					jsonObject1.put("content", row.stringValue("TW_CONTENT"));
					jsonObject1.put("env", row.stringValue("TW_ENV_NAME"));
					jsonArray1.put(jsonObject1);
				}
			}else{
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("content", "无");
				jsonArray1.put(jsonObject1);
			}
			
			DataRow currentDayworkRecord = wmDayworkManage.findCurrentDayworkRecords(userId,currentTime);
			JSONArray jsonArray2 = new JSONArray();
			if(currentDayworkRecord != null){
				JSONObject jsonObject2 = new JSONObject();
				String weekText = DateUtil.getWeekText(currentDayworkRecord.getTimestamp("TW_TIME"));
				jsonObject2.put("time", currentDayworkRecord.stringValue("TW_TIME").substring(0, 10));
				jsonObject2.put("weekText", weekText);
				jsonObject2.put("id", currentDayworkRecord.stringValue("TW_ID"));
				jsonObject2.put("content", currentDayworkRecord.stringValue("TW_CONTENT"));
				jsonObject2.put("env", currentDayworkRecord.stringValue("TW_ENV_NAME"));
				jsonArray2.put(jsonObject2);
			}else{
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("content", "无");
				jsonArray2.put(jsonObject2);
			}
			
			List<DataRow> fllowDayworkRecords = wmDayworkManage.findFllowDayworkRecords(userId,maxTime);
			JSONArray jsonArray3 = new JSONArray();
			if(fllowDayworkRecords.size() != 0){
				int size = 5;
				if(fllowDayworkRecords.size() < 5){
					size = fllowDayworkRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = fllowDayworkRecords.get(i);
					JSONObject jsonObject3 = new JSONObject();
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					jsonObject3.put("time", row.stringValue("TW_TIME").substring(0, 10));
					jsonObject3.put("weekText", weekText);
					jsonObject3.put("id", row.stringValue("TW_ID"));
					jsonObject3.put("content", row.stringValue("TW_CONTENT"));
					jsonObject3.put("env", row.stringValue("TW_ENV_NAME"));
					jsonArray3.put(jsonObject3);
				}
			}else{
				JSONObject jsonObject3 = new JSONObject();
				jsonObject3.put("content", "无");
				jsonArray3.put(jsonObject3);
			}
			
			jsonObject.put("past", jsonArray1);
			jsonObject.put("currentday", jsonArray2);
			jsonObject.put("follow", jsonArray3);
			responseText = jsonObject.toString();
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer createDayWorkRecord(DataParam param){
		String responseText = FAIL;
		try {
        	String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
    		String twtimeUTC = jsonObject.get("twtime").toString().replace("Z", " UTC");
    		Date sd = format.parse(twtimeUTC);
    		String twtime = DateUtil.format(DateUtil.YYMMDDHHMISS_HORIZONTAL, sd);
    		
        	String twenv = jsonObject.get("twenv").toString();//环境
        	String twcontent = jsonObject.get("twcontent").toString();//内容
        	
        	WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
        	
        	User user = (User)this.getUser();
        	String userId = user.getUserId();
        	
        	DataRow row = wmDayworkManage.findCurrentDayworkRecords(userId, twtime);
        	
        	if(row == null){
        		DataParam dataParam = new DataParam();
            	dataParam.put("TW_ID", KeyGenerator.instance().genKey());
            	dataParam.put("USER_ID", userId);
            	dataParam.put("TW_TIME", twtime);
            	dataParam.put("TW_ENV", twenv);
            	dataParam.put("TW_CONTENT", twcontent);
            	wmDayworkManage.createDayWorkRecord(dataParam);
            	
            	responseText = SUCCESS;
        	}else{
        		responseText = ERROR;
        	}
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer delWorkDailyInfo(DataParam param){
		String responseText = FAIL;
		try {
        	String dailyWorkId = param.get("currentId");
        	if(!dailyWorkId.isEmpty()){
        		WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
            	wmDayworkManage.delDayWorkRecord(dailyWorkId);
            	responseText = SUCCESS;
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getWorkDailyInfo(DataParam param){
		String responseText = FAIL;
		try {
        	String dailyWorkId = param.get("currentId");
        	if(!dailyWorkId.isEmpty()){
        		WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
            	DataRow workDailyRow = wmDayworkManage.getWorkDailyInfoRecord(dailyWorkId);
            	String weekText = DateUtil.getWeekText(workDailyRow.getTimestamp("TW_TIME"));
            	JSONObject jsonObject = new JSONObject();
            	jsonObject.put("twtime", workDailyRow.stringValue("TW_TIME").substring(0, 10));
            	jsonObject.put("weekText", weekText);
            	jsonObject.put("id", workDailyRow.stringValue("TW_ID"));
            	jsonObject.put("twcontent", workDailyRow.stringValue("TW_CONTENT"));
            	String twenv = workDailyRow.stringValue("TW_ENV");
            	if("InOffice".equals(twenv)){
            		jsonObject.put("twenv", "公司办公");
            	}else if("BusinessOffice".equals(twenv)){
            		jsonObject.put("twenv", "出差办公");
            	}else if("HomeOffice".equals(twenv)){
            		jsonObject.put("twenv", "在家办公");
            	}else{
            		jsonObject.put("twenv", "");
            	}
            	
			
            	responseText = jsonObject.toString();
        	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer updateDayWorkRecord(DataParam param){
		String responseText = FAIL;
		try {
        	String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	String dailyWorkId = jsonObject.get("id").toString();
        	String twenv = jsonObject.get("twenv").toString();//环境
        	String twcontent = jsonObject.get("twcontent").toString();//内容
        	
        	WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
        	wmDayworkManage.updateDayWorkRecord(dailyWorkId,twenv,twcontent);
        	
        	responseText = SUCCESS;
        	
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findWorkDailiesExaminationListInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = param.get("id");
			if("null".equals(userId) || userId == null){
				userId = user.getUserId();
			}
			String minTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.findFllowDayworkRecords(userId,minTime);
			JSONArray jsonArray11 = new JSONArray();
			if(records.size() != 0){
				for(int j=0;j<records.size();j++){
					DataRow row = records.get(j);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("time", row.stringValue("TW_TIME").substring(5, 10));
					jsonObject11.put("weekText", weekText);
					jsonObject11.put("id", row.stringValue("TW_ID"));
					jsonObject11.put("content", row.stringValue("TW_CONTENT"));
					jsonObject11.put("env", row.stringValue("TW_ENV_NAME"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
//				jsonObject11.put("id","无");
//				jsonObject11.put("content","无");
//				jsonArray11.put(jsonObject11);
			}
			jsonObject.put("dayworks", jsonArray11);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initExaminationUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			List<DataRow> userRecords = wmDayworkManage.initUserRecords(grpId);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userId", row.stringValue("USER_ID"));
				jsonObject1.put("userName",row.stringValue("USER_NAME"));
				jsonObject1.put("userCode", row.stringValue("USER_CODE"));
				jsonArray1.put(jsonObject1);
				jsonArray2.put(row.stringValue("USER_CODE"));
			}
			jsonObject.put("userInfos", jsonArray1);
			jsonObject.put("userCodes", jsonArray2);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			List<DataRow> userRecords = wmDayworkManage.findUserRecords();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userId", row.stringValue("USER_ID"));
				jsonObject1.put("userName",row.stringValue("USER_NAME"));
				jsonObject1.put("userCode", row.stringValue("USER_CODE"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("userInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initDayExamGroupInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			List<DataRow> groupRecords = wmDayworkManage.getMngGroupRecords(userId);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<groupRecords.size();i++){
				DataRow row = groupRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("grpId", row.stringValue("GRP_ID"));
				jsonObject1.put("grpName",row.stringValue("GRP_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("groupInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initCurrentDayInfo(DataParam param){
		String responseText = FAIL;
		try {
			String userId = param.get("userId");
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			DataRow row = wmDayworkManage.findCurrentDayworkRecords(userId,currentDate);
			JSONArray jsonArray11 = new JSONArray();
			if(row != null){
				String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
				JSONObject jsonObject11 = new JSONObject();
				String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
				jsonObject11.put("time", time);
				jsonObject11.put("weekText", weekText);
				jsonObject11.put("id", row.stringValue("TW_ID"));
				jsonObject11.put("content", row.stringValue("TW_CONTENT"));
				jsonObject11.put("env", row.stringValue("TW_ENV_NAME"));
				jsonArray11.put(jsonObject11);
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("content","无");
				jsonArray11.put(jsonObject11);
			}
			jsonObject.put("dayworks", jsonArray11);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initPastDaysInfo(DataParam param){
		String responseText = FAIL;
		try {
			String userId = param.get("userId");
			String minTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.findPastDayWorkRecords(userId,minTime);
			JSONArray jsonArray11 = new JSONArray();
			if(records.size() != 0){
				for(int j=0;j<records.size();j++){
					DataRow row = records.get(j);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject jsonObject11 = new JSONObject();
					String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
					jsonObject11.put("time", time);
					jsonObject11.put("weekText", weekText);
					jsonObject11.put("id", row.stringValue("TW_ID"));
					jsonObject11.put("content", row.stringValue("TW_CONTENT"));
					jsonObject11.put("env", row.stringValue("TW_ENV_NAME"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("content","无");
				jsonArray11.put(jsonObject11);
			}
			jsonObject.put("dayworks", jsonArray11);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initFllowDayInfo(DataParam param){
		String responseText = FAIL;
		try {
			String userId = param.get("userId");
			String maxTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1));
			
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			JSONObject jsonObject = new JSONObject();
			List<DataRow> records = wmDayworkManage.findFllowDayworkRecords(userId,maxTime);
			JSONArray jsonArray11 = new JSONArray();
			if(records.size() != 0){
				for(int j=0;j<records.size();j++){
					DataRow row = records.get(j);
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					JSONObject jsonObject11 = new JSONObject();
					String time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, row.getTimestamp("TW_TIME"));
					jsonObject11.put("time", time);
					jsonObject11.put("weekText", weekText);
					jsonObject11.put("id", row.stringValue("TW_ID"));
					jsonObject11.put("content", row.stringValue("TW_CONTENT"));
					jsonObject11.put("env", row.stringValue("TW_ENV_NAME"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("content","无");
				jsonArray11.put(jsonObject11);
			}
			jsonObject.put("dayworks", jsonArray11);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findActiveUserId(DataParam param){
		String responseText = FAIL;
		try {
			String userCode = param.get("userCode");
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			DataRow userInfo = wmDayworkManage.findActiveUserId(userCode);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("userId", userInfo.stringValue("USER_ID"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
}
