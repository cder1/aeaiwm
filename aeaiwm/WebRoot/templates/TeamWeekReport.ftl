<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Created>2006-09-16T00:00:00Z</Created>
  <LastSaved>2015-08-14T06:17:26Z</LastSaved>
  <Version>14.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
  <RemovePersonalInformation/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7980</WindowHeight>
  <WindowWidth>14805</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>135</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s62">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s63">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s64">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s65">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#0070C0"/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#FF0000"/>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#FFFFFF"
    ss:Bold="1"/>
   <Interior ss:Color="#00B050" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#FFFFFF"
    ss:Bold="1"/>
   <Interior ss:Color="#00B050" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s71">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11"/>
  </Style>
  <Style ss:ID="s72">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#00B050"/>
  </Style>
  <Style ss:ID="s75">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s76">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
  </Style>
 </Styles>
 <#list logs as log>
 <Worksheet ss:Name="${log.item}周总结">
  <Table ss:ExpandedColumnCount="7" ss:ExpandedRowCount="9999999" x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="54" ss:DefaultRowHeight="13.5">
   <Column ss:AutoFitWidth="0" ss:Width="51.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="207"/>
   <Column ss:AutoFitWidth="0" ss:Width="60"/>
   <Column ss:AutoFitWidth="0" ss:Width="82.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="64.5"/>
   <Column ss:Width="58.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="109.5"/>
   
   <Row ss:AutoFitHeight="0" ss:Height="18">
    <Cell ss:StyleID="s68"><Data ss:Type="String">分类</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">内容说明</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">负责人</Data></Cell>
    <Cell ss:StyleID="s69"><Data ss:Type="String">工作量（天）</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">开始时间</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">当前状态</Data></Cell>
    <Cell ss:StyleID="s68"><Data ss:Type="String">原因说明</Data></Cell>
   </Row>
    <#list log.weekSummaryStart as summaryStart>
   <Row>
    <Cell ss:MergeDown="${log.weekSummaryIndex}" ss:StyleID="s75"><Data ss:Type="String">本周总结</Data></Cell>
    <Cell ss:Index="2" ss:StyleID="s63"><Data ss:Type="String">${summaryStart.content}</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String">${summaryStart.user}</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="Number">${summaryStart.day}</Data></Cell>
    <Cell ss:StyleID="s65"><Data ss:Type="String">${summaryStart.date}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${summaryStart.status}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${summaryStart.reason}</Data></Cell>
   </Row>
   </#list>
     <#list log.weekSummary as summary>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s63"><Data ss:Type="String">${summary.content}</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String">${summary.user}</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="Number">${summary.day}</Data></Cell>
    <Cell ss:StyleID="s65"><Data ss:Type="String">${summary.date}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${summary.status}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${summary.reason}</Data></Cell>
   </Row>
   </#list>
    <#list log.weekPlanStart as planStart>
   <Row>
  	<Cell ss:MergeDown="${log.weekPlanIndex}" ss:StyleID="s76"><Data ss:Type="String">下周计划</Data></Cell>
    <Cell ss:Index="2" ss:StyleID="s64"><Data ss:Type="String">${planStart.content}</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String">${planStart.user}</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="Number">${planStart.day}</Data></Cell>
    <Cell ss:StyleID="s65"><Data ss:Type="String">${planStart.date}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${planStart.status}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${planStart.reason}</Data></Cell>
   </Row>
   </#list>
   <#list log.weekPlan as plan>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s64"><Data ss:Type="String">${plan.content}</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String">${plan.user}</Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="Number">${plan.day}</Data></Cell>
    <Cell ss:StyleID="s65"><Data ss:Type="String">${plan.date}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${plan.status}</Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String">${plan.reason}</Data></Cell>
    </Row>
   </#list>
    <Row>
    <Cell ss:StyleID="s76"><Data ss:Type="String">后续考虑</Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s63"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s62"><Data ss:Type="Number"></Data></Cell>
 	<Cell ss:StyleID="s65"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s71"><Data ss:Type="String"></Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
  </#list>
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>0</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>17</ActiveRow>
     <ActiveCol>8</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
